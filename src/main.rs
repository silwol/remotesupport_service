#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate uuid;

mod schema;
mod models;

use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use uuid::Uuid;

pub fn create_device<'a>(
    conn: &PgConnection,
    id: &'a Uuid,
    devicetype: &'a str,
) -> models::Device {
    use schema::devices;

    let new_device = models::NewDevice { id, devicetype };

    diesel::insert_into(devices::table)
        .values(&new_device)
        .get_result(conn)
        .expect("Error saving new post")
}

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url =
        env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

fn main() {
    println!("Hello, world!");
    let connection = establish_connection();

    create_device(&connection, &Uuid::new_v4(), &"absorption");
}
