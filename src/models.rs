extern crate uuid;

use super::schema::devices;

use uuid::Uuid;

#[derive(Queryable)]
pub struct Device {
    pub id: Uuid,
    pub devicetype: String,
}

#[derive(Insertable)]
#[table_name = "devices"]
pub struct NewDevice<'a> {
    pub id: &'a Uuid,
    pub devicetype: &'a str,
}
